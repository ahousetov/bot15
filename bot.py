# -*- coding: utf-8 -*-
import config
import telebot
import logging
import time
from telebot import apihelper
#apihelper.proxy = {'https': 'socks5://getmobit:{}@104.236.22.137:1080'.format('12qw!@QW')}

bot = telebot.TeleBot(config.token)
logger = logging.getLogger('LOGGER')
#telebot.logger.setLevel(logging.DEBUG)


@bot.message_handler(commands=['start', 'help'], content_types=['text'])
def repeat_all_messages(message):
    bot.send_message(message.chat.id, message.text)



if __name__ == '__main__':
    try:
        logger.error('Попытка подключения....')
        bot.poll()
    except (KeyboardInterrupt, SystemExit):
        raise
    except Exception as e:
        logger.error('Ошибка подключения:{}'.format(e))
        time.sleep(5)
